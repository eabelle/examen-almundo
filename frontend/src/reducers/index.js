import { combineReducers } from 'redux'
import hotels from './hotels'

const reducers = combineReducers({
  hotels
})

export default reducers