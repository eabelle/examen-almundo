import { FETCH_HOTELS, SET_DEFAULT_HOTELS } from '../actions'

const hotels = (state = [], action) => {
  switch (action.type) {
    case FETCH_HOTELS:
      return action.payload
    case SET_DEFAULT_HOTELS:
      return []
    default:
      return state
  }
}

export default hotels