import React, { Component } from 'react';
import Filters from './components/Filters'
import Hotels from './components/Hotels'
import './App.css';

const LOGO_ALMUNDO_URL = '/images/logo-almundo.svg';

class App extends Component {

  render() {
    return (
      <div className="App" style={{height: window.height}}>
        <header className="App-header">
          <img src={LOGO_ALMUNDO_URL} className="App-logo" alt="logo" />
        </header>
        <div className="row App-content">
          <Filters />
          <Hotels />
        </div>
      </div>
    );
  }
}

export default App;
