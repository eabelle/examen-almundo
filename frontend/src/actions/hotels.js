import Axios from 'axios'
import qs from 'qs'
import { FETCH_HOTELS, SET_DEFAULT_HOTELS } from '.'

export function loadHotelsSuccess(hotels) {  
  return {
        type: FETCH_HOTELS,
        payload: hotels
    };
}

export function loadHotelsFail() {  
  return {
        type: SET_DEFAULT_HOTELS,
  };
}

export function fetchHotels(filters) {

  return (dispatch) => {
    return Axios.get(`/api/?${qs.stringify(filters)}`)
    .then(response => dispatch(loadHotelsSuccess(response.data)))
    .catch(err => {
      alert(err)
      return dispatch(loadHotelsFail())
    });
  }
}