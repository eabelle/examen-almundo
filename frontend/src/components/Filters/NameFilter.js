import React from 'react';
import FilterBox from './FilterBox'

const SEARCH_LOGO_URL = '/icons/filters/search.svg'

const NameFilter = ({handleNameChange, fetchHotels}) => {
    
    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            fetchHotels();
        }
    }

    return (
        <FilterBox name="Nombre de hotel" logo={SEARCH_LOGO_URL} collapseId="collapse-name" >
            <div className="name-filter-container">
                <input 
                    type="text" 
                    className="name-filter-input" 
                    onChange={handleNameChange} 
                    onKeyPress={handleKeyPress}
                    placeholder="Ingrese el nombre del hotel"
                />
                <button 
                    type="button" 
                    id="name-filter-button" 
                    className="btn btn-primary" 
                    onClick={fetchHotels}
                >
                    Aceptar
                </button>
            </div>
        </FilterBox>
    )
}

export default NameFilter