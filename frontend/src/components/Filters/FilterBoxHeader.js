import React from 'react'

const FilterBoxHeader = ({name, logo, collapsed, isTitle}) => (
    <div className="filter-title-box">
        {
            isTitle
            ? <span className="filters-container-title">{ name }</span>
            : (
                <span>
                    <span className="filter-logo-container">
                        <img src={logo} className="filter-logo" alt="logo" height={14} />
                    </span>
                    <span className="filter-name">{ name }</span>
                </span>
            ) 
        }
        <span className={ collapsed ? "arrow-down" : "arrow-up" } > </span>
    </div>
)

export default FilterBoxHeader