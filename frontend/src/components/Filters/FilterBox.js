import React from 'react';
import Collapser from '../Collapser'
import FilterBoxHeader from './FilterBoxHeader'
import './style.css';

class FilterBox extends React.Component {

    state = {
      collapsed: false
    }

    render() {  
      return (
          <div className="card">
            <div className="card-content">
              <Collapser 
                  selector={this.props.collapseId}
                  onClick={()=>{this.setState({collapsed: !this.state.collapsed})}}
              >
                <FilterBoxHeader logo={this.props.logo } name={this.props.name} collapsed={this.state.collapsed} />
              </Collapser>
              <div className="collapse show card-content" id={this.props.collapseId}>
                {this.props.children}
              </div>
            </div>
          </div>
      )
    }
}

export default FilterBox;