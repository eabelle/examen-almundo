import React from 'react'
import StarsIndicator from '../StarsIndicator'

const StarsSelector = ({quantity, selected, onChange, name}) => {
    return (
        <div className="stars-selector-container" >
            <input type="checkbox" 
            className="stars-selector-checkbox" 
            checked={selected}
            onChange={onChange}
            name={name}
            />
            {
                quantity === -1
                ? <span className="stars-selector-label stars-selector-label-all">Todas las estrellas</span>
                : <span className="stars-selector-label" >
                    <StarsIndicator quantity={quantity} height={14} />
                </span>
            }
        </div>
    )
}  

export default StarsSelector