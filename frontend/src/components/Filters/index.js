import React from 'react';
import {isEqual} from 'lodash'
import { connect } from 'react-redux'
import { fetchHotels } from '../../actions/hotels'
import StarsFilter from './StarsFilter.js'
import NameFilter from './NameFilter.js'
import Collapser from '../Collapser'
import FilterBoxHeader from './FilterBoxHeader'
import './style.css';

const DEFAULT_STARS = {
  "1": false,
  "2": false,
  "3": false,
  "4": false,
  "5": false,
  "all": true
}

const EMPTY_STARS = {
  "1": false,
  "2": false,
  "3": false,
  "4": false,
  "5": false,
  "all": false
}

const DEFAULT_STATE = {
  stars: DEFAULT_STARS,
  name: "",
  collapsed: false
}

class Filters extends React.Component {

  state = DEFAULT_STATE

  componentWillMount() {
    this.props.fetchHotels()
  }

  fetchHotels = () => {
    this.props.fetchHotels({
      name: this.state.name,
      stars: Object.keys(this.state.stars).filter((key) => (
        key !== 'all' && this.state.stars[key]
      ))
    })
  }

  handleStarsChange = (event) => {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;

      if(name==='all') {
          this.setState({stars: DEFAULT_STARS}, () => {this.fetchHotels()})
      } else {
          this.setState({
            stars: Object.assign ({}, this.state.stars, {
              [name]: value,
              all: false
            })
          }, () => {
            if(isEqual(this.state.stars, EMPTY_STARS)) {
              this.setState({stars: DEFAULT_STARS}, () => {this.fetchHotels()})
            } else {
              this.fetchHotels()
            }
          })
      }
  }

  handleNameChange = (event) => {
    this.setState({name: event.target.value})
  }

  render() {
    return(
        <div className="col-xs-12 col-md-4 filters-container">
            <div className="card">
              <Collapser
                selector={`collapsable-filters`}
                onClick={()=>{this.setState({collapsed: !this.state.collapsed})}}
              >
                <FilterBoxHeader name={"Filtros"} collapsed={this.state.collapsed} isTitle />
              </Collapser>
            </div>
            <div className="collapse show" id="collapsable-filters">
              <NameFilter fetchHotels={this.fetchHotels} handleNameChange={this.handleNameChange} />
              <StarsFilter stars={this.state.stars}  handleStarsChange={this.handleStarsChange} />
            </div>
        </div>
    )  
  }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchHotels: (filter) => {
      dispatch(fetchHotels(filter))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters)