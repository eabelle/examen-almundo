import React from 'react'
import './style.css';
import StarsSelector from './StarsSelector'
import FilterBox from './FilterBox'

const STAR_LOGO_URL = '/icons/filters/star.svg'

class StarsFilter extends React.Component {

    render() {
        return (
          <FilterBox name="Estrellas" logo={STAR_LOGO_URL} collapseId="collapse-stars">
              <div className="form-check">
                <StarsSelector quantity={-1} selected={this.props.stars.all} onChange={this.props.handleStarsChange} name="all" />  
                <StarsSelector quantity={5} selected={this.props.stars["5"]} onChange={this.props.handleStarsChange} name="5" />  
                <StarsSelector quantity={4} selected={this.props.stars["4"]} onChange={this.props.handleStarsChange} name="4" />  
                <StarsSelector quantity={3} selected={this.props.stars["3"]} onChange={this.props.handleStarsChange} name="3" />  
                <StarsSelector quantity={2} selected={this.props.stars["2"]} onChange={this.props.handleStarsChange} name="2" />  
                <StarsSelector quantity={1} selected={this.props.stars["1"]} onChange={this.props.handleStarsChange} name="1" />  
              </div>
          </FilterBox>
        )
    }
}

export default StarsFilter;