import React from 'react'
const STAR_LOGO_URL = '/icons/filters/star-golden.svg'

const StarsIndicator = ({quantity, height}) => {
    let stars = []
    for(let i = 0; i < quantity; i++) {
        stars.push(
            <img 
                src={STAR_LOGO_URL} 
                alt="logo" 
                height={height} 
                key={i} 
            />
        )
    }
    return stars
}

export default StarsIndicator