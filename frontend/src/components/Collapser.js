import React from 'react'

const Collapser = ({selector, children, onClick}) => (
    <a 
        data-toggle="collapse" 
        href={`#${selector}`} 
        aria-expanded="false" 
        aria-controls={selector}
        onClick={onClick}
    >
        {children}
    </a>
)

export default Collapser;