import React from 'react'
import './style.css'

const AMENITIES_URL = '/icons/amenities'

const Amenities = ({amenities}) => {
    return amenities.map(amenity => {
        return <img 
            className="amenity"
            src={`${AMENITIES_URL}/${amenity}.svg`} 
            alt="amenity" 
            height={18} 
            key={amenity} 
        />
    })
}

export default Amenities