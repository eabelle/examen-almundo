import React from 'react';

const PriceBox = ({price}) => {
  return (
    <div className="price-box">
      <span className="price-header">Precio por noche por habitación</span>
      <span className="price-container">
        <span className="price-currency">{" ARS "}</span>
        <span className="price-value">{price}</span>
      </span>
      <button type="button" className="btn btn-primary watch-hotel-button">Ver Hotel</button>
    </div>
  )
}

export default PriceBox