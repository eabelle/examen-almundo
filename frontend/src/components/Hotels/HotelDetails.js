import React from 'react';
import './style.css';
import StarsIndicator from '../StarsIndicator'
import Amenities from './Amenities'

const HotelDetails = ({name, stars, amenities}) => {
  return(
        <div className="hotelDetailsContainer">
            <div className="hotelDetail">
                {name}
            </div>
            <div className="hotelDetail">
                <StarsIndicator quantity={stars} height={18} />
            </div>
            <div className="hotelDetail">
                <p />
                <Amenities amenities={amenities}/>
            </div>
        </div>
  )  
}

export default HotelDetails