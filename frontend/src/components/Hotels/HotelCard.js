import React from 'react';
import HotelDetails from './HotelDetails'
import PriceBox from './PriceBox'

const IMAGES_URL = '/images/hotels'

const HotelCard = ({image, name, stars, amenities, price}) => (
    <div className="card hotel-container" >
        <div style={{width: "100%"}}>
            <img 
            className="card-img-top hotel-card-image" 
            src={`${IMAGES_URL}/${image}`} 
            alt="Hotel" 
            />
            <HotelDetails name={name} stars={stars} amenities={amenities} />
            <PriceBox price={price} />
        </div>
    </div>
)

export default HotelCard;