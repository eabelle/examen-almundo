import React from 'react';
import HotelCard from './HotelCard'
import { connect } from 'react-redux'
import './style.css';

const Hotels = ({hotels}) => {
  return(
    <div className="col-xs-12 col-md-8 hotels-list-container">
      { hotels.map( hotel => <HotelCard {...hotel} key={hotel.id} />) }
    </div>
  )  
}

Hotels.defaultProps = {
  hotels: []
}

const mapStateToProps = (state, ownProps) => {
  return {
    hotels: state.hotels
  }
}

export default connect(mapStateToProps)(Hotels);