import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { shallow } from 'enzyme';

describe('The App', () => {

  it('should render a div', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.type()).toBe('div')
  })

  it('should contain a header', () => {
    const wrapper = shallow(<App />);
    const header = wrapper.find('header')
    expect(header).toHaveLength(1)
    expect(header.hasClass('App-header')).toBe(true)    
  })

  it('should contain one div as app content', () => {
    const wrapper = shallow(<App />);
    const content = wrapper.find('.App-content')
    expect(content).toHaveLength(1)
    expect(content.is('div')).toBe(true)    
  })

  describe('The App content', () => {

    it('should render the filters', () => {
      const wrapper = shallow(<App />);
      const content = wrapper.find('.App-content')
      expect(content.find('Connect(Filters)')).toHaveLength(1)
    })

    it('should render the hotels', () => {
      const wrapper = shallow(<App />);
      const content = wrapper.find('.App-content')
      expect(content.find('Connect(Hotels)')).toHaveLength(1)
    })

  })

})
