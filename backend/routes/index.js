var express = require('express');
var router = express.Router();
var data = require('../data/data.json');

/* GET home page. */
router.get('/', function(req, res, next) {
  
  if(req.query.name || req.query.stars) {

    var filteredData =  data.filter(function(hotel) {
      return (
        (req.query.name ? hotel.name.includes(req.query.name) : true) && 
        (req.query.stars ? req.query.stars.find(stars => parseInt(stars) === hotel.stars) > 0 : true)
      )
    });

    res.send(filteredData)
    return
  };

  res.send(data);

});

module.exports = router;
