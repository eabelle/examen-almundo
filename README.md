# Examen almundo.com

A continuación, se muestran las instrucciones para instalar y correr tanto el backend como el frontend de la aplicación.

### Backend

- Ir a la carpeta `./backend` del proyecto
- Ejecutar los siguientes comandos
```
npm install
npm start
```

El backend quedará corriendo por defecto en la dirección `http://localhost:3001/`

### Frontend

- Ir a la carpeta `./frontend` del proyecto
- Ejecutar `npm install`
- En caso de querer especificar una url para la API de backend, dentro de la carpeta `./frontend`, ir al archivo `package.json` y modificar el siguiente slice del archivo con la url deseada:
```
{
    ...

    "proxy": "http://localhost:3001"
    
    ...
}
```
- Ejecutar `npm start` para correr el servidor.

El frontend quedará corriendo por defecto en la dirección `http://localhost:3000`, y expone por proxy un endpoint para la API de backend, el cual es `/api`.

---

*Quedan como tareas pendientes los tests unitarios, y los puntos opcionales, los cuales consistirían en principio en una capa de DAO y un DTO para el esquema de los hoteles, y los métodos de la capa de rutas del backend para el CRUD de los mismos.